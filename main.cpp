#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <queue>

using namespace std;
class Beer{
private:
    int beerPrice;
    int beerName;
public:
    Beer(int beerName, int beerPrice) : beerName(beerName), beerPrice(beerPrice) {

    }

    bool operator==(const Beer &rhs) const {
        return beerPrice == rhs.beerPrice &&
               beerName == rhs.beerName;
    }

    bool operator!=(const Beer &rhs) const {
        return !(rhs == *this);
    }

    bool operator<(const Beer &rhs) const {
        if (beerPrice < rhs.beerPrice)
            return true;
        if (rhs.beerPrice < beerPrice)
            return false;
        return beerName < rhs.beerName;
    }

    bool operator>(const Beer &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const Beer &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const Beer &rhs) const {
        return !(*this < rhs);
    }

    int getBeerPrice() const {
        return beerPrice;
    }

    int getBeerName() const {
        return beerName;
    }

};
class Restaurant{
public:
    int restaurantName;
    int orderBeerQuantity;
    set<Restaurant *> neighbours;
    set<Restaurant *> neighboursBackup;
    const Beer * beerToSell= nullptr;

public:
    Restaurant(int restaurantName, int orderBeerQuantity) : restaurantName(restaurantName),
                                                            orderBeerQuantity(orderBeerQuantity) {

    }

    void addNeighbour(Restaurant * neighbour){
        neighbours.insert(neighbour);
        neighboursBackup.insert(neighbour);
    }
    void deleteNeighbour(Restaurant * neighbour){
        neighbours.erase(neighbour);
    }
    int sellBeer(set<Beer> beerToSell){
        for (auto neigbour : neighboursBackup){
            if(neigbour->hasSoldBeer()){
                beerToSell.erase(*neigbour->soldBeer());
            }
        }
        this->beerToSell= new Beer(beerToSell.begin().operator*().getBeerName(), beerToSell.begin().operator*().getBeerPrice());
        return orderBeerQuantity*=this->beerToSell->getBeerPrice();
    }
    const Beer * soldBeer()const {
        return beerToSell;
    }
    bool hasSoldBeer(){
        return beerToSell!= nullptr;
    }


};
struct CustomCompare {
    bool operator()( Restaurant *rhs1, Restaurant *rhs2) const {
        if (rhs2->orderBeerQuantity < rhs1->orderBeerQuantity)
            return true;
        else if (rhs2->orderBeerQuantity < rhs1->orderBeerQuantity)
            return false;
        else if (rhs2->neighbours.size() > rhs1->neighbours.size())
            return true;
        else if (rhs2->neighbours.size() < rhs1->neighbours.size())
            return false;
        else if (rhs2->restaurantName < rhs1->restaurantName)
            return false;
        else
            return true;

    }
};
class Network{
    int                         numOfRestaurants;
    int                         numOfBeers;
    set<Beer>                   beers;
    map<int,Restaurant*>        restaurantsOrderdByName;
    multimap<int,Restaurant*,greater<>>   restaurantsOrderdByVolumeOfBeer;
    set<Restaurant* , CustomCompare>            restaurantOrderComplex;

public:

    Network(int numOfRestaurants, int numOfBeers) : numOfRestaurants(numOfRestaurants), numOfBeers(numOfBeers) {
    }
    void addBeer(int name , int price ){
    beers.insert(Beer(name,price));
    }
    void addRestaurant(int name , int beerOrderAmmount){
        auto newRestaurants=new Restaurant(name, beerOrderAmmount);
        restaurantsOrderdByName.insert(pair<int,Restaurant*>(name,newRestaurants));
        restaurantsOrderdByVolumeOfBeer.insert(pair<int,Restaurant*>(beerOrderAmmount,newRestaurants));
    }
    void addPath(int restaurant1,int restaurant2){
        restaurantsOrderdByName.find(restaurant1)->second->addNeighbour(restaurantsOrderdByName.find(restaurant2)->second);
        restaurantsOrderdByName.find(restaurant2)->second->addNeighbour(restaurantsOrderdByName.find(restaurant1)->second);
    }

    multimap<int,Restaurant*,greater<>> restaurantsByOrderAmmount(){
        return restaurantsOrderdByVolumeOfBeer;
    }
    set<Restaurant*, CustomCompare> * restaurantsByOrderComplex(){
        return &restaurantOrderComplex;
    }
    map<int,Restaurant*> * restaurantsByName(){
        return &restaurantsOrderdByName;
    }
    set<Beer> avalibleBeers(){
        return beers;
    }
    int getNumOfRestaurants() const {
        return numOfRestaurants;
    }

    void createComplexSort(){
        for(auto restaurant:restaurantsOrderdByName){
            restaurantOrderComplex.insert(restaurant.second);
        }
    }

};
Network getInput(){
    int numOfRestaurants;
    int numOfBeers;
    cin>>numOfRestaurants>>numOfBeers;
    Network newNetwork(numOfRestaurants,numOfBeers);
    int beerCost;
    for(int i=0;i<numOfBeers;i++){
        cin>>beerCost;
        newNetwork.addBeer(i+1,beerCost);
    }
    int beerOrderAmmount;
    for(int i=0;i<numOfRestaurants;i++){
        cin>>beerOrderAmmount;
        newNetwork.addRestaurant(i+1,beerOrderAmmount);
    }
    int restaurant1,restaurant2;
    for(int i=1;i<numOfRestaurants;i++){
        cin>>restaurant1>>restaurant2;
        newNetwork.addPath(restaurant1,restaurant2);
    }
    return newNetwork;
}
void printOutput(Network  &network, int lowestPrice) {
        cout << lowestPrice << endl;
        auto itr=network.restaurantsByName()->begin();

        for (int i = 0; i <network.getNumOfRestaurants()-1; i++,itr++) {
            cout << itr->second->soldBeer()->getBeerName() << " ";

        }
        cout << itr->second->soldBeer()->getBeerName();
        cout << endl;
    }


void findLowestPrice(Network  &network,int & lowestPrice,queue<Restaurant*> restaurantQue){
    vector<int> BNFR;
    while(!restaurantQue.empty()){
        lowestPrice+=restaurantQue.front()->sellBeer(network.avalibleBeers());
        restaurantQue.pop();
    }
//    for(auto restaturant : *network.restaurantsByOrderComplex()){
//        lowestPrice+=restaturant->sellBeer(network.avalibleBeers());
//    }

}
void sort(map<int,Restaurant*> neighbours,queue<Restaurant*> & sorted){
    int neighboursWeight=0;
    queue<int> aboutToDelete;

    while(!neighbours.empty()) {
        for (auto neighbour : neighbours) {
            neighboursWeight = -1;
            for (auto nej : neighbour.second->neighbours) {
                neighboursWeight += nej->orderBeerQuantity;
            }
            if (neighboursWeight <= neighbour.second->orderBeerQuantity) {

                aboutToDelete.push(neighbour.first);
                sorted.push(neighbour.second);
                for (auto nej : neighbour.second->neighbours) {
                    nej->deleteNeighbour(neighbour.second);
                }
                break;
            }
        }
        while (!aboutToDelete.empty()) {
            neighbours.erase(aboutToDelete.front());
            aboutToDelete.pop();
        }
    }

}

int main() {
    auto network = getInput();
    int lowestPrice=0;
    network.createComplexSort();
    queue<Restaurant*> sorted;
    sort(*network.restaurantsByName(),sorted);
    findLowestPrice(network,lowestPrice,sorted);
    printOutput(network,lowestPrice);
    return 0;
}